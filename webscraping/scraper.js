

// - - - - - SCRAPE JS CODE  - - - - - //
 
    const cheerio = require('cheerio');
    const puppeteer = require('puppeteer');
    
    const url = 'https://en.wikipedia.org/wiki/Portal:Current_events';

// - - - - - examples of webpages + HTML identifiers:  - - - - - //
// https://en.wikipedia.org/wiki/Portal:Current_events  > .description
// https://en.wikipedia.org/wiki/Portal:Current_events — b > a
// https://trends.google.com/trends/trendingsearches/realtime?geo=GB&category=all' > .title
// https://trends.google.com/trends/trendingsearches/daily?geo=GB > details-wrapper
// https://www.reddit.com/r/news/ > h3
    
    puppeteer
      .launch()
      .then(browser => browser.newPage())
      .then(page => {
        return page.goto(url).then(function() {
          return page.content();
        });
      })
      .then(html => {
        const $ = cheerio.load(html);
        const newsHeadlines = [];
        $('b > a').each(function() {
          newsHeadlines.push($(this).text());
        });
        console.log(newsHeadlines);
      })
      .catch(console.error);


// for returning objects:
  //newsHeadlines.push({
    // title: $(this).text(),
  //});

// code adapted from source ref.: https://pusher.com/tutorials/web-scraper-node/

// - - - - - - - - - -  - - - - - - - - - -  - - - - - - - - - -  - - - - - - - - - - //

// - - - - - sequence of steps in the terminal:  - - - - - //

// cat scraper.js
// node scraper.js

// - - - copy paste array from the terminal to the webconsole and replace existing array:
//   var voices = [
//      "newsHeadlines 01",
//      "newsHeadlines 02",
//      "newsHeadlines X"
//   ];

// - - replace source in the iframe:
//   document.querySelector("iframe").src = 'https://en.wikipedia.org/wiki/Portal:Current_events';

// - - replace iframe array
//   var dance = [ 
//      "https://trends.google.com/trends/trendingsearches/realtime?geo=GB&category=all", 
//      "https://www.reddit.com/r/news/"
//    ];

// - - add to existing array:
//    dance.push("https://www.reddit.com/r/news/");

